#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 18 11:43:21 2018

@author: mlworkstation
"""

import json
from Bio import Entrez

def search(query):
    Entrez.email = 'your.email@example.com'
    handle = Entrez.esearch(db='pubmed', 
                            sort='relevance', 
                            retmax='20',
                            retmode='xml', 
                            term=query)
    results = Entrez.read(handle)
    return results

def fetch_details(id_list):
    ids = ','.join(id_list)
    Entrez.email = 'your.email@example.com'
    handle = Entrez.efetch(db='pubmed',
                           retmode='xml',
                           id=ids)
    results = Entrez.read(handle)
    return results

if __name__ == '__main__':
    # parameters of search are vital
    # in this case, sarch query is 'drug interactions'
    results = search('drug interactions')
    
    # other queries
    q0 = 'drug interactions'
    q1 = '("drug interactions"[TIAB] NOT Medline[SB]) OR  \
    "drug interactions"[MeSH Terms] OR  \
    drug interaction[Text word]'
    q2 = 'AND ("Toxicity Tests"[MeSH] OR \
    "Adverse Drug Reaction Reporting \
    Systems"[MeSH] OR \
    "Drug Hypersensitivity"[MeSH] OR \
    "Drug Antagonism"[MeSH] OR \
    "drugs, investigational"[MeSH] OR \
    "Drug evaluation"[MeSH] OR \
    "adverse effects"[Subheading] OR \
    "toxicity"[Subheading] OR \
    "poisoning"[Subheading] OR \
    "chemically induced"[Subheading] OR \
    "contraindications"[Subheading])'
    
    # results from other queries
    results0 = search(q0)
    results1 = search(q1)
    results2 = search(q2)
    
    id_list = results['IdList']
    papers = fetch_details(id_list)['PubmedArticle']
    books = fetch_details(id_list)['PubmedBookArticle']
    
    # Print titles
    for i, paper in enumerate(papers):
        #print(i+1, paper['MedlineCitation']['Article']['ArticleTitle'])
        print(i+1, paper['MedlineCitation']['Article']['ArticleTitle'])
    
    # Pretty print the first paper in full
    print(json.dumps(papers[0], indent=2, separators=(',', ':')))
    
    # Pretty print the first book-paper in full
    print(json.dumps(books[0], indent=2, separators=(',', ':')))